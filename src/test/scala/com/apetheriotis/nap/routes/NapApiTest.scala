package com.apetheriotis.nap.routes

import com.apetheriotis.nap.rest.RestRoutes
import org.mockserver.integration.ClientAndServer._
import org.mockserver.model.Header
import org.mockserver.model.HttpRequest._
import org.scalatest.FlatSpec

//import org.specs2.mutable.Specification

import spray.testkit.ScalatestRouteTest

import scala.io.Source

class NapApiTest extends FlatSpec with RestRoutes with ScalatestRouteTest {
  def actorRefFactory = system

  // Set bigger timeout because by default it's set to 1s
  val timeoutz = new scala.concurrent.duration.FiniteDuration(60, scala.concurrent.duration.SECONDS)

  implicit def default: NapApiTest.this.type#RouteTestTimeout = RouteTestTimeout(timeoutz)

  // Start mock server
  val externalsApisResponse = Source.fromURL(getClass.getResource("/exampleExternalProductsResponse.txt")).getLines().mkString
  val mockServer = startClientAndServer(18082)
  mockServer.when(request()
    .withPath("/products"))
    .respond(org.mockserver.model.HttpResponse.response(externalsApisResponse)
    .withHeaders(new Header("Content-Type", "application/json"))
    )

  override def afterAll(): Unit = {
    mockServer.stop()
  }


  "The api" should "return valid response when query matches products" in {
    Get("/api/v1/products?q=Stripe") ~> apiRoutes ~> check {
      val expectedResponse = Source.fromURL(getClass.getResource("/exampleApiResponseWithStripeProducts.txt")).getLines().mkString
      assert(response.status.intValue == 200)
      assert(response.entity.asString.equalsIgnoreCase(expectedResponse))
    }
  }
  it should "return empty data when no results" in {
    Get("/api/v1/products?q=foobar") ~> apiRoutes ~> check {
      val expectedResponse = Source.fromURL(getClass.getResource("/exampleApiResponseWithNoProducts.txt")).getLines().mkString
      assert(response.status.intValue == 200)
      assert(response.entity.asString.equalsIgnoreCase(expectedResponse))
    }
  }

}
