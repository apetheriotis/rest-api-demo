package com.apetheriotis.nap.services

import org.scalatest.{Matchers, WordSpecLike}

class ProductsFilterSpecs extends WordSpecLike with Matchers {

  "A query" must {
    "should match product when query is contained in name" in {
      val productName = "Stripe-trimmed open-knit cotton-blend sweater"
      val query = "Stripe"
      val isMatch = ProductsFilter.filterProduct(productName,query)
      isMatch shouldBe true
    }
    "should match product when query is contained in name regardeless of the camel case" in {
      val productName = "STRipe-trimmed open-knit cotton-blend sweater"
      val query = "striPE"
      val isMatch = ProductsFilter.filterProduct(productName,query)
      isMatch shouldBe true
    }
    "should not match product when query is not contained as one word" in {
      val productName = "STRipe-trimmed open-knit cotton-blend sweater"
      val query = "cotton-blend"
      val isMatch = ProductsFilter.filterProduct(productName,query)
      isMatch shouldBe false
    }
    "should not match product when query is not a exact word of the product name" in {
      val productName = "stripe-trimmed open-knit cotton-blend sweater"
      val query = "strip"
      val isMatch = ProductsFilter.filterProduct(productName,query)
      isMatch shouldBe false
    }
  }


}
