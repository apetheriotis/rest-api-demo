package com.apetheriotis.nap.services

import akka.actor.{ActorSystem, Props}
import akka.testkit._
import com.apetheriotis.nap.dto.NapProduct
import com.apetheriotis.nap.exceptions.SourceUnavailableException
import com.apetheriotis.nap.protocol.{GetProducts, ListNapProducts}
import org.mockserver.integration.ClientAndServer._
import org.mockserver.model.Header
import org.mockserver.model.HttpRequest._
import org.mockserver.model.HttpResponse._
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpecLike}

import scala.io.Source

/**
 * Specs for [[com.apetheriotis.nap.services.ProductsManagementService]]
 */
class ProductsManagementServiceSpecs extends TestKit(ActorSystem("testActorSystem")) with ImplicitSender
with DefaultTimeout with WordSpecLike with Matchers with StopSystemAfterAll
with BeforeAndAfterEach {

  // Start mock server
  val externalsApisResponse = Source.fromURL(getClass.getResource("/exampleExternalProductsResponse.txt")).getLines().mkString
  val mockServer = startClientAndServer(18083)
  mockServer.when(request()
    .withPath("/products"))
    .respond(response().withHeaders(new Header("Content-Type", "application/json"))
    .withBody(externalsApisResponse))

  override def afterAll(): Unit = {
    mockServer.stop()
  }

  "An ProductsManagementService" must {
    "should throw exception when connection to external API is not available" in {
      val serverUrl = "http://localhost:18083/productz"
      val query = "stripe"
      val actorRef = TestActorRef(Props(new ProductsManagementService(serverUrl)))
      intercept[SourceUnavailableException] {
        actorRef.receive(GetProducts(query))
      }
      expectNoMsg()
    }
    "should filter products consumed from the external API" in {
      val serverUrl = "http://localhost:18083/products"
      val query = "stripe"
      val actorRef = TestActorRef(Props(new ProductsManagementService(serverUrl)))
      actorRef ! GetProducts(query)
      val products = List(new NapProduct("428236", "Stripe-trimmed open-knit cotton-blend sweater", 515))
      expectMsg(ListNapProducts(products))
    }
  }


}
