package com.apetheriotis.nap.dto

/**
 * ErrorMetadata that should be send in a response in case of error when request processing
 * @param code the status code or error code
 * @param errorMessage the error message if any
 */
case class Metadata(code: Integer, errorMessage: Option[String]) {
}
