package com.apetheriotis.nap.dto

/**
 * The product object
 */
case class NapProduct(productId: String, name: String, price: Int) {
}
