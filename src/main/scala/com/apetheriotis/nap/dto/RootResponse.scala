package com.apetheriotis.nap.dto

/**
 * The root response. Every response from API should be wrapped within RootResponse object
 * @param metadata the ErrorMetadata object if any
 * @param data the result object
 */
case class RootResponse(metadata: Metadata = Metadata(200, None), data: Option[AnyRef]) {

}

