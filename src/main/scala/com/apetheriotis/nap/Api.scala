package com.apetheriotis.nap

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.apetheriotis.nap.rest.RestServiceActor
import spray.can.Http

/**
 * Created by angelos on 14/10/2015.
 */
object Api {

  def main(args: Array[String]) {
    implicit val system = ActorSystem("NapActorSystem")

    // Start the rest api
    val rest = system.actorOf(Props[RestServiceActor])
    IO(Http) ! Http.Bind(rest, interface = "0.0.0.0", port = 18081)

  }
}
