package com.apetheriotis.nap.services


object ProductsFilter {

  /**
   * Filter on product's name based on the provided search query
   */
  def filterProduct(productName: String, query: String): Boolean = {
    val productNameTerms = productName.split("[-\\s]").map(_.toLowerCase)
    val matchedTerms = productNameTerms.filter(term => term.equalsIgnoreCase(query.toLowerCase))
    if (matchedTerms.length > 0) true else false
  }
}
