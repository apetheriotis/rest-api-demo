package com.apetheriotis.nap.services

import akka.actor.{Actor, ActorLogging, Props}
import com.apetheriotis.nap.dto.NapProduct
import com.apetheriotis.nap.exceptions.SourceUnavailableException
import com.apetheriotis.nap.protocol.{GetProducts, ListNapProducts}
import com.typesafe.config.ConfigFactory
import org.json4s.DefaultFormats

import scalaj.http.{Http, HttpOptions}

/**
 * Companion object to properly initiate [[com.apetheriotis.nap.services.ProductsManagementService]]
 */
object ProductsManagementService {
  def props(): Props = {
    // We manually pass the url here (even though we could directly to the actor) for easier testing
    val url = ConfigFactory.load().getString("productsApi.url")
    Props(new ProductsManagementService(url))
  }
}

/**
 * An actor responsible for managing requests related to products
 */
class ProductsManagementService(externalApiUrl: String) extends Actor with ActorLogging {

  val MAX_PRODUCTS_TO_EXAMINE = 10000
  val TIMEOUT_EXTERNAL_REQUEST = 10000

  def receive = {
    // --- Get products from external server -> convert to required format -> send back --- //
    case GetProducts(query) =>
      val json = getProductsFromExternalApiAsJson
      val filter = ProductsFilter.filterProduct(_: String, query)
      val products = convertAndFilterProducts(json, filter)
      sender() ! ListNapProducts(products)
  }

  /**
   * @return the json data of the products as received from the external API
   */
  def getProductsFromExternalApiAsJson: String = {
    val rs = Http(externalApiUrl).header("Content-Type", "application/json").header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(TIMEOUT_EXTERNAL_REQUEST)).execute()
    if (!rs.is2xx) throw new SourceUnavailableException()
    rs.body
  }


  /**
   * Filter products received from the external API by applying the given filter function
   */
  def convertAndFilterProducts(json: String, filterFunction: String => Boolean): List[NapProduct] = {
    implicit val formats = DefaultFormats
    import org.json4s.JsonAST._
    import org.json4s.jackson.JsonMethods._

    val externalProducts = (parse(json) \ "data").extract[JArray].children
    externalProducts
      .filter(ep => filterFunction((ep \ "name" \ "en").extract[String]))
      .take(MAX_PRODUCTS_TO_EXAMINE)
      .map(ep => new NapProduct(
      productId = (ep \ "id").extract[String],
      name = (ep \ "name" \ "en").extract[String],
      price = (ep \ "price" \ "gross").extract[Int] / (ep \ "price" \ "divisor").extract[Int]))
  }


}