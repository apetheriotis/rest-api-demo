package com.apetheriotis.nap.rest

import akka.actor.Actor
import spray.routing.HttpService

/**
 * Defines what are all the available routes and a custom exception handler
 */
class RestServiceActor extends Actor with HttpService with CustomExceptionHandler with RestRoutes {

  def actorRefFactory = context

  def receive = runRoute(handleExceptions(customExceptionHandler)(apiRoutes))

}
