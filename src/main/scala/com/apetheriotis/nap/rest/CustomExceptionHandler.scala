package com.apetheriotis.nap.rest

import com.apetheriotis.nap.dto.{Metadata, RootResponse}
import com.apetheriotis.nap.exceptions.CustomException
import spray.http.MediaTypes
import spray.httpx.Json4sSupport
import spray.routing._

/**
 * Custom exception handler to format error responses according to API guidelines
 */
trait CustomExceptionHandler extends HttpService with Json4sSupport {
  implicit val customExceptionHandler = ExceptionHandler {
    case e: CustomException =>
      respondWithMediaType(MediaTypes.`application/json`) {
        complete(e.statusCode, RootResponse(Metadata(e.code,Some(e.message)),None))
      }
  }
}