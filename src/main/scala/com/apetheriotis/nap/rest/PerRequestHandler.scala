package com.apetheriotis.nap.rest

import akka.actor.{Props, _}
import com.apetheriotis.nap.dto.RootResponse
import com.apetheriotis.nap.protocol.Protocol
import com.apetheriotis.nap.rest.PerRequestHandler.WithProps
import org.json4s.{DefaultFormats, Formats}
import spray.http.StatusCodes._
import spray.httpx.Json4sSupport
import spray.routing.RequestContext

import scala.concurrent.duration._

/**
 * Implements a per request actor handler
 */
trait PerRequestHandler extends Actor with Json4sSupport with ActorLogging {

  import context._

  /* Use to mix Spray's Marshalling Support with json4s */
  implicit def json4sFormats: Formats = DefaultFormats

  /* Timeout for each request */
  setReceiveTimeout(15.seconds)

  def r: RequestContext

  def target: ActorRef

  def message: Protocol

  target ! message

  def receive = {
    case res: Protocol =>
      r.complete(OK, RootResponse(data = Some(res)))
      stop(self)
    case ReceiveTimeout =>
      r.complete(RequestTimeout)
      stop(self)
      throw new RuntimeException("Timeout!")
  }

}

object PerRequestHandler {

  case class WithProps(r: RequestContext, props: Props, message: Protocol) extends PerRequestHandler {
    lazy val target = context.actorOf(props)
  }

}

trait PerRequestCreator {
  def perRequest(actorRefFactory: ActorRefFactory, r: RequestContext, props: Props, message: Protocol) =
    actorRefFactory.actorOf(Props(new WithProps(r, props, message)))
}
