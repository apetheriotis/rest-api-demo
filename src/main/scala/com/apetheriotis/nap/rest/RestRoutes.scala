package com.apetheriotis.nap.rest

import akka.util.Timeout
import com.apetheriotis.nap.protocol.{GetProducts, ProductsProtocol}
import com.apetheriotis.nap.services.ProductsManagementService
import org.json4s.DefaultFormats
import spray.httpx.Json4sSupport
import spray.routing._

import scala.concurrent.duration._

/**
 * Rest API
 */
trait RestRoutes extends HttpService with Json4sSupport with PerRequestCreator {


  implicit def json4sFormats: DefaultFormats.type = DefaultFormats

  implicit val timeout = Timeout(10.seconds)

  /* Api routes*/
  val apiRoutes =
    path("api" / "v1" / "products") {
      parameters('q) { (query) => {
        get {
          handleProductsRequest(GetProducts(query))
        }
      }
      }
    }


  def handleProductsRequest(message: ProductsProtocol): Route =
    ctx => perRequest(actorRefFactory, ctx, ProductsManagementService.props(), message)


}
