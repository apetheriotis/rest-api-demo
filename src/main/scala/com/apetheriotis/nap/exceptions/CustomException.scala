package com.apetheriotis.nap.exceptions

import spray.http.StatusCode

/**
 * Custom exception which holds a message and a specific code ID and the corresponding http status code
 * for every type of exception
 */
trait CustomException extends RuntimeException {
  var message: String
  var code: Integer
  var statusCode: StatusCode
}

