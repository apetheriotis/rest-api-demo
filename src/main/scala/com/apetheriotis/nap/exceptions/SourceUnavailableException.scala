package com.apetheriotis.nap.exceptions

import spray.http.{StatusCodes, StatusCode}

/**
 * Exception that should be thrown when source api is not available
 */
class SourceUnavailableException() extends CustomException {
  override var message: String = s"Source API is not available."
  override var code: Integer = 9001
  override var statusCode: StatusCode = StatusCodes.InternalServerError
}
