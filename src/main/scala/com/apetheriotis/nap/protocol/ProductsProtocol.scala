package com.apetheriotis.nap.protocol

import com.apetheriotis.nap.dto.NapProduct


/**
 * Describes the messages needed for products retrieval
 */
sealed trait ProductsProtocol extends Protocol

/**
 * Message to update new sensor
 */
case class GetProducts(query: String) extends ProductsProtocol

/**
 * Message that holds products
 */
case class ListNapProducts(products:List[NapProduct]) extends  ProductsProtocol