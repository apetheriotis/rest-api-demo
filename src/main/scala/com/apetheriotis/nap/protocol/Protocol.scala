package com.apetheriotis.nap.protocol

/**
 * Trait for the messages needed for communication between actors
 */
trait Protocol
