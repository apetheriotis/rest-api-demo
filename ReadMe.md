### NAP Rest API
The rest API is built using scala / spray / akka using 'actor per request model' to take advantage of timeout management / easier testing / seperation of logic from routing / better error handling. For testing we used scalatest and mockserver-netty to mock requests to external products API.

## How to compile and run
* mvn clean install (runs tests as well)
* java -jar target/nap-1.0-SNAPSHOT-fat.jar 
* http://localhost:18081/api/v1/products?q=Fringed

## Things to note
* The provided url format "http://localhost:port/q=query-string" is not well formatted, so I used other endpoint as described above.
* The provided response is not well formated (no root attribute) so I wrapped the response in a format that in my opinion is better.
* Probably I have understand incorrectly the parsing of the search query and the patterns to match so there might be some incosistencies in the actual data returned.